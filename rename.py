#!/usr/bin/env python3

import os
import sys
import re

path = sys.argv[1]
pattern = sys.argv[2]
filer = re.compile(pattern)
extr = re.compile(r"(\.\w+)$")

for file in sorted(os.listdir(path)):
    if not os.path.isfile(file):
        continue
    filem = filer.search(file)
    if not filem:
        continue
    extm = extr.search(file)
    new_name = f"{filem.group(1)}{extm.group(1)}"
    print(f"{file} => {new_name}")
    os.rename(os.path.join(path, file), os.path.join(path, new_name))
