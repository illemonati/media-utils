#!/usr/bin/env python3

import os
import sys
import subprocess
import threading
import time


def download_from_link(download_link, dir_path):
    with open(os.devnull, 'wb') as devnull:
        print(f'Starting download of {download_link}')
        proc = subprocess.Popen(
               ['aria2c', '--seed-time=0', f"-d{dir_path}", download_link], stdout=devnull, stderr=devnull)
        proc.wait()
        print(f'Done Downloading {download_link}')


def download(download_file, dir_path):
    with open(download_file, 'r') as file:
        for line in file.readlines():
            if line in ['', '\n']:
                continue
            thread = threading.Thread(target=download_from_link, args=(line[:-1], dir_path))
            thread.start()
            time.sleep(1)

def main():
    download_file = sys.argv[1]
    dir_path = sys.argv[2]
    download(download_file, dir_path)

if __name__ == '__main__':
    main()
